#pragma execution_character_set("utf-8")

#include "Scenes.h"

USING_NS_CC;

cocos2d::Scene* TitleScene::createScene()
{
    return TitleScene::create();
}

bool TitleScene::init()
{
    if (!Scene::init())
    {
        return false;
    }
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

    auto backButton = MenuItemImage::create(
        "Play.png",
        "Play.png",
        CC_CALLBACK_1(TitleScene::mainMenuCallback, this));

    float x = origin.x + visibleSize.width - backButton->getContentSize().width / 2;
    float y = origin.y + backButton->getContentSize().height / 2;
    backButton->setPosition(Vec2(x, y));

    auto menu = Menu::create(backButton, NULL);
    menu->setPosition(Vec2::ZERO);
    this->addChild(menu, 1);

    auto label = Label::createWithTTF("たいとる がめん", "fonts/yojo.ttf", 24);
    if (label != nullptr)
    {
        label->setPosition(Vec2(origin.x + visibleSize.width / 2,
            origin.y + visibleSize.height - label->getContentSize().height));
        this->addChild(label, 1);
    }


    // 作成したパーティクルのプロパティリストを読み込み
    ParticleSystemQuad* particle = ParticleSystemQuad::create("particle/mouse_texture.plist");
    //パーティクルのメモリーリーク回避（★重要）
    particle->setAutoRemoveOnFinish(true);
    // パーティクルを開始
    particle->resetSystem();
    // パーティクルを配置
    this->addChild(particle);

    //マウスイベント設定
    auto mouseListener = EventListenerMouse::create();
    mouseListener->onMouseMove = [=](Event* event) {
        auto mouse = (EventMouse*)event;
        particle->setPosition(Point(mouse->getCursorX(), mouse->getCursorY()));
    };
    _eventDispatcher->addEventListenerWithSceneGraphPriority(mouseListener, this);

    return true;
}

void TitleScene::mainMenuCallback(cocos2d::Ref* pSender)
{
    auto scene = MainMenuScene::createScene();
    TransitionFade* fade = TransitionFade::create(0.5f, scene, Color3B::WHITE);
    Director::getInstance()->replaceScene(fade);
}
