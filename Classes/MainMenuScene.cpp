#pragma execution_character_set("utf-8")

#include "Scenes.h"

USING_NS_CC;

cocos2d::Scene* MainMenuScene::createScene()
{
    return MainMenuScene::create();
}

bool MainMenuScene::init()
{
    if (!Scene::init())
    {
        return false;
    }
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

    auto backButton = MenuItemImage::create(
        "Close.png",
        "Close.png",
        CC_CALLBACK_1(MainMenuScene::backCallback, this));

    float x = origin.x + visibleSize.width - backButton->getContentSize().width / 2;
    float y = origin.y + backButton->getContentSize().height / 2;
    backButton->setPosition(Vec2(x, y));

    auto menu = Menu::create(backButton, NULL);
    menu->setPosition(Vec2::ZERO);
    this->addChild(menu, 1);

    auto label = Label::createWithTTF("めいんめにゅー", "fonts/yojo.ttf", 24);
    if (label != nullptr)
    {
        label->setPosition(Vec2(origin.x + visibleSize.width / 2,
            origin.y + visibleSize.height - label->getContentSize().height));
        this->addChild(label, 1);
    }

    return true;
}

void MainMenuScene::backCallback(cocos2d::Ref* pSender)
{
    auto scene = TitleScene::createScene();
    TransitionFade* fade = TransitionFade::create(0.5f, scene, Color3B::WHITE);
    Director::getInstance()->replaceScene(fade);
}
